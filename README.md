# README #

You device doesn't let you change the Media Volume unless a media is already being played?
And you always wanted to do it?

Your problems are now over, with the Media Volume Controller you can easily access the media volume controller with just one touch!

Simply click the application icon and the control bar will appear in your screen, now all you have to do is change the volume!

Awesome!

### Media Volume Controller ###

* Easily access the media volume controller with Media Volume Controller.
* 1.0
* [Google Play](https://play.google.com/store/apps/details?id=com.rodrigocamara.mediacontroller)
* [YouTube](https://youtu.be/-wVGOs1kysw)
* [Blogger PT-BR](http://roarenacamara.blogspot.com.br/2015/12/controlando-o-volume-de-midia-em.html)

### Who do I talk to? ###

* Repo owner (Rodrigo Câmara - rodrigoarenacamara@gmail.com)