package com.rodrigocamara.mediacontroller;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {
    private AudioManager mAudioManager;
    private int previousVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public void onStart() {
        super.onStart();
        showMediaVolumeController();
        finish();
    }

    private void showMediaVolumeController() {
        if (!mAudioManager.isMusicActive()) {
            previousVolume = mAudioManager.getStreamVolume(3);
            mAudioManager.setStreamVolume(3, previousVolume, 1);
            return;
        } else {
            Toast.makeText(getBaseContext(), R.string.toast, Toast.LENGTH_LONG).show();
            return;
        }
    }
}
